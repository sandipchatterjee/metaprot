from django.contrib import admin

# Register your models here.

from .models import Microcloud, MongosNodes

admin.site.register(Microcloud)
admin.site.register(MongosNodes)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbstats', '0006_auto_20150618_2018'),
    ]

    operations = [
        migrations.CreateModel(
            name='MongosNodes',
            fields=[
                ('hostname', models.CharField(serialize=False, primary_key=True, max_length=100)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('available', models.BooleanField()),
            ],
        ),
    ]

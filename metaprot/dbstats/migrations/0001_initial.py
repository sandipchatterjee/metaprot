# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Microcloud',
            fields=[
                ('node', models.IntegerField(primary_key=True, serialize=False)),
                ('hostname', models.CharField(max_length=50)),
                ('last_updated', models.DateField(auto_now=True)),
                ('free_space', models.IntegerField()),
                ('available', models.BooleanField()),
            ],
        ),
    ]

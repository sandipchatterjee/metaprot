# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbstats', '0005_auto_20150618_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='microcloud',
            name='last_updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]

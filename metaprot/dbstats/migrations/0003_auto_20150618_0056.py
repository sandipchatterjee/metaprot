# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbstats', '0002_microcloud_data_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='microcloud',
            name='free_space',
            field=models.BigIntegerField(),
        ),
    ]

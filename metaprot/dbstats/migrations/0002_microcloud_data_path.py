# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbstats', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='microcloud',
            name='data_path',
            field=models.CharField(default='/', max_length=250),
            preserve_default=False,
        ),
    ]

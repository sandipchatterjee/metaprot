from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import HttpResponseRedirect

from .models import Microcloud,MongosNodes
from .forms import RefreshForm
from .tasks import *

import datetime

# Create your views here.

def status(request):

    all_microclouds = Microcloud.objects.all().order_by('node')
    all_mongos = MongosNodes.objects.all().order_by('hostname')
    
    context = {'microcloud': all_microclouds, 'mongos_nodes': all_mongos}

    if request.method == 'POST':
        form = RefreshForm(request.POST)
        
        result = update_all_nodes.delay()
        result2 = update_mongos_status.delay()
        context['updating'] = True
        messages.add_message(request, messages.INFO, "Updating...")
        return redirect(reverse('dbstats:status'))

    else:
        form = RefreshForm()

    context['updating'] = False
    return render(request, 'dbstats/status.html', context)

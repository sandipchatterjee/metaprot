from __future__ import absolute_import

import time
import subprocess
from celery import shared_task
from .models import Microcloud, MongosNodes

@shared_task
def add(x, y):
    return x + y

@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)

def update_node(node_object):
    node_hostname = node_object.hostname
    path = node_object.data_path

    ssh_command = ['ssh', node_hostname, 'df -h --output=avail {}'.format(path)]
    try:
        output = subprocess.check_output(ssh_command, universal_newlines=True)
    except subprocess.CalledProcessError:
        return None
    free_space_bytes = output.split('\n')[1]
    node_object.free_space = free_space_bytes
    
    mongo_command = ['mongo', node_hostname, '--eval', 'db.isMaster()']
    status = subprocess.call(mongo_command)
    if status == 0:
        node_object.available = True
    else:
        node_object.available = False
    node_object.save()
    return True

@shared_task
def update_all_nodes():
    all_nodes = Microcloud.objects.all()
    for node in all_nodes:
        if update_node(node):
            pass
        else:
            node.available = False
            node.save()

    return all_nodes

@shared_task
def update_mongos_status():
    all_nodes = MongosNodes.objects.all()
    
    node_dict = {node.hostname:node for node in all_nodes}

    node_names = ','.join(node_dict.keys())
    ssh_command = ['ssh', 'sandip@garibaldi', 'for i in {{{}}}; do mongo --quiet --host $i --port 27018 --eval "sh.status()" > /dev/null 2>&1; echo "$i $?"; done'.format(node_names)]

    try:
        output = subprocess.check_output(ssh_command, universal_newlines=True)
    except subprocess.CalledProcessError:
        return output

    node_statuses = [node.split() for node in output.split('\n')]
    for node_status in node_statuses[:-1]:
        node_name = node_status[0]
        node_avail = node_status[1]
        if node_avail == '0':
            node_dict[node_name].available = True
        else:
            node_dict[node_name].available = False
        node_dict[node_name].save()

    return node_statuses

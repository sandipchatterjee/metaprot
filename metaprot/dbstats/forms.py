from django import forms

class RefreshForm(forms.Form):
    refresh = forms.CharField(widget=forms.HiddenInput())

from django.db import models

# Create your models here.

class Microcloud(models.Model):
    node = models.IntegerField(primary_key=True)
    hostname = models.CharField(max_length=50)
    last_updated = models.DateTimeField(auto_now=True)
    free_space = models.CharField(max_length=30)
    data_path = models.CharField(max_length=250)
    available = models.BooleanField()

    def __str__(self):
        return self.hostname

class MongosNodes(models.Model):
    hostname = models.CharField(max_length=100, primary_key=True)
    last_updated = models.DateTimeField(auto_now=True)
    available = models.BooleanField()

    def __str__(self):
        return self.hostname

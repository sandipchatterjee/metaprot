from django.shortcuts import render
from django.views.generic import TemplateView, ListView, CreateView, DeleteView, DetailView

from .models import Plot

class PlotListView(ListView):
    
    model = Plot

    def get_context_data(self, **kwargs):
        context = super(PlotListView, self).get_context_data(**kwargs)
        # context additions here
        return context



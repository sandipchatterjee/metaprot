# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=500)),
                ('supported_file_type', models.CharField(choices=[('DTA', 'DTA'), ('SQT', 'SQT'), ('MS1', 'MS1'), ('MS2', 'MS2')], max_length=3)),
                ('last_modified', models.DateTimeField(null=True, auto_now=True)),
            ],
        ),
    ]

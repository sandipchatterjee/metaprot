from django.db import models

class Plot(models.Model):

    """Table representing available analyses and their params/info"""

    FILE_TYPE_CHOICES = (
        ('DTA', 'DTA'),
        ('SQT', 'SQT'),
        ('MS1', 'MS1'),
        ('MS2', 'MS2'),
    )

    name = models.CharField(max_length=200)
    description = models.TextField(max_length=500)
    supported_file_type = models.CharField(max_length=3, choices=FILE_TYPE_CHOICES)
    last_modified = models.DateTimeField(auto_now=True, null=True)


# -*- coding: utf-8 -*-
from django import forms
from .models import Document

class DocumentForm(forms.ModelForm):

    """Represents an uploaded document file
        (DTASelect-filter.txt, SQT, MS1, MS2)
    """

    class Meta:
        model = Document
        fields = ['docfile', 'file_type', 'description', 'slug']


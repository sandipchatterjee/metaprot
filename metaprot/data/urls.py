from django.conf.urls import url
from django.core.urlresolvers import reverse, reverse_lazy
from .views import (
            DocumentListView, DTAListView, SQTListView, MS1ListView, MS2ListView, 
            DTADetailView, SQTDetailView, MS1DetailView, MS2DetailView,
            DTAParseView, DTAJSONView,
            DocumentCreateView, 
            DocumentDeleteView,
)

urlpatterns = [
                url(r'^$', DocumentCreateView.as_view(), name='index'),
                url(r'^(?P<pk>\d+)/delete$', DocumentDeleteView.as_view(), name='delete'),
#                url(r'^delete/(?P<pk>\d+)/$', DocumentDeleteView.as_view(), name='delete'),
                url(r'^dta/$', DTAListView.as_view(), name='DTA'),
                url(r'^sqt/$', SQTListView.as_view(), name='SQT'),
                url(r'^ms1/$', MS1ListView.as_view(), name='MS1'),
                url(r'^ms2/$', MS2ListView.as_view(), name='MS2'),
                url(r'^dta/(?P<pk>\d+)/$', DTADetailView.as_view(), name='DTA-detail'),
                url(r'^sqt/(?P<pk>\d+)/$', SQTDetailView.as_view(), name='SQT-detail'),
                url(r'^ms1/(?P<pk>\d+)/$', MS1DetailView.as_view(), name='MS1-detail'),
                url(r'^ms2/(?P<pk>\d+)/$', MS2DetailView.as_view(), name='MS2-detail'),
                url(r'^dta/(?P<pk>\d+)/json/$', DTAJSONView.as_view(), name='DTA-json'),
                url(r'^dta/(?P<pk>\d+)/parse/$', DTAParseView.as_view(), name='DTA-parse'),
]

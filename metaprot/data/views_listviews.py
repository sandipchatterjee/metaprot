# ListViews defined in here to keep things more organized...

from django.views.generic import ListView

from .models import Document


class DocumentListView(ListView):
    model = Document
    context_object_name = 'documents'

class DTAListView(DocumentListView):
    context_object_name = 'dta_documents'
    template_name_suffix = '_dta_list'
    queryset = Document.objects.filter(file_type='DTA').order_by('-last_modified')

class SQTListView(DocumentListView):
    context_object_name = 'sqt_documents'
    template_name_suffix = '_sqt_list'
    queryset = Document.objects.filter(file_type='SQT').order_by('-last_modified')

class MS1ListView(DocumentListView):
    context_object_name = 'ms1_documents'
    template_name_suffix = '_ms1_list'
    queryset = Document.objects.filter(file_type='MS1').order_by('-last_modified')

class MS2ListView(DocumentListView):
    context_object_name = 'ms2_documents'
    template_name_suffix = '_ms2_list'
    queryset = Document.objects.filter(file_type='MS2').order_by('-last_modified')


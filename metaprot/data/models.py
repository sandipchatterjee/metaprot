import os
from base64 import b16encode
from django.db import models

def _create_hash():
    """ returns a random hash of length 20 """
    return b16encode(os.urandom(10)).decode('utf-8')

def _content_file_name(instance, filename):
    name = _create_hash() + os.path.splitext(filename)[1]
    return '/'.join(['data', name])

class Document(models.Model):

    """Model for a generic uploaded proteomics file (MS1, MS2, SQT, DTASelect-filter.txt)
    """

    FILE_TYPE_CHOICES = (
        ('DTA', 'DTA'),
        ('SQT', 'SQT'),
        ('MS1', 'MS1'),
        ('MS2', 'MS2'),
    )

    docfile = models.FileField(upload_to=_content_file_name)
    #private_hash = models.CharField(max_length=20, null=True, default=_create_hash, unique=True)
    file_type = models.CharField(max_length=3, null=True, choices=FILE_TYPE_CHOICES)
    description = models.TextField(max_length=500, blank=True)
    last_modified = models.DateTimeField(auto_now=True, null=True)
    #created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    slug = models.SlugField(max_length=200, blank=True)

#    class Meta:
#        managed = False

    def __str__(self):
        return self.docfile.name

    def save(self, *args, **kwargs):
        self.slug = self.docfile.name
        super(Document, self).save(*args, **kwargs)

class DTAFile(models.Model):
    document_id = models.OneToOneField(Document, unique=True, primary_key=True)
    parsed = models.BooleanField(default=False)
    parsed_json = models.FileField(upload_to='documents/%Y/%m/%d', null=True)
    last_processed = models.DateTimeField(auto_now=True, null=True)


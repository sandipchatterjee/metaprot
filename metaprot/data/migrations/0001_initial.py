# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('docfile', models.FileField(upload_to='documents/%Y/%m/%d')),
                ('file_type', models.CharField(choices=[('DTA', 'DTA'), ('SQT', 'SQT'), ('MS1', 'MS1'), ('MS2', 'MS2')], null=True, max_length=3)),
                ('description', models.TextField(max_length=500, blank=True)),
                ('last_modified', models.DateTimeField(auto_now=True, null=True)),
                ('slug', models.SlugField(max_length=200, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DTAFile',
            fields=[
                ('document_id', models.OneToOneField(to='data.Document', primary_key=True, serialize=False)),
                ('parsed', models.BooleanField(default=False)),
                ('parsed_json', models.FileField(upload_to='documents/%Y/%m/%d', null=True)),
                ('last_processed', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import data.models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0002_auto_20150714_0159'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='document',
            name='private_hash',
            field=models.CharField(default=data.models._create_hash, max_length=20, unique=True, null=True),
        ),
    ]

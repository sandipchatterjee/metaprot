# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0004_remove_document_private_hash'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='created',
        ),
    ]

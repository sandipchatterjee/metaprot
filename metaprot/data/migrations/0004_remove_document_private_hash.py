# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_auto_20150714_0206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='private_hash',
        ),
    ]

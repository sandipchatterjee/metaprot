#from django.shortcuts import render

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages

from django.views.generic import TemplateView, ListView, CreateView, DeleteView, DetailView

from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response as RestResponse # to prevent potential name conflicts
from rest_framework.views import APIView
from rest_framework import status as rest_status # to prevent potential name conflicts

from .views_listviews import *
from .forms import DocumentForm
from .models import Document, DTAFile

import os
import datetime

class DocumentCreateView(CreateView):
    model = Document
    form_class = DocumentForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.handle_uploaded_file(self.request.FILES['docfile'])
        self.object.save()
        return HttpResponseRedirect(reverse_lazy('data:index'))

    def handle_uploaded_file(self, f): ## can probably remove this.
        with open('_file_upload', 'wb') as destination:
            for chunk in f.chunks():
                destination.write(chunk)

    def form_invalid(self, form):
        return HttpResponse(content=form, status=400, content_type='application/json')

    def get_context_data(self, **kwargs):
        """build queryset for CreateView...
        """
        
        kwargs['object_list'] = Document.objects.all().order_by('-last_modified')[:5]
        return super(DocumentCreateView, self).get_context_data(**kwargs)

class DocumentDeleteView(DeleteView):
    model = Document
    success_url = reverse_lazy('data:index')
    # should also delete associated files from DTAFile, MS2File, etc. if available (parsed JSON, for example)

class DocumentDetailView(DetailView):
    model = Document

    def get_context_data(self, **kwargs):
        context = super(DocumentDetailView, self).get_context_data(**kwargs)

        properties_list = []
        properties_list.append(('File type', context['object'].file_type))
        properties_list.append(('Details', context['object'].description))
        properties_list.append(('File size (MB)', self.convert_file_size(context['object'].docfile.size)))
        properties_list.append(('File location', context['object'].docfile.url))
        properties_list.append(('File uploaded', context['object'].last_modified))
        context['properties'] = properties_list

        return context

    def convert_file_size(self, file_size_bytes):
        """Converts input size in bytes to megabytes (string)"""
        return '{0:.3f}'.format(file_size_bytes/(1024**2))+' MB'


class DTADetailView(DocumentDetailView):

    """DetailView for DTASelect-filter.txt files
    """

    template_name_suffix = '_dta_detail'

    def get_context_data(self, **kwargs):
        context = super(DTADetailView, self).get_context_data(**kwargs)

        try:
            dta_record = DTAFile.objects.get(pk=self.kwargs['pk'])
        except DTAFile.DoesNotExist:
            # if no DTAFile instance exists for this file
            # and the file is a DTA file, create a new DTAFile instance
            if context['object'].file_type == 'DTA':
                dta_record = DTAFile(pk=self.kwargs['pk']) ## add other init params if desired
                dta_record.save()
            else:
                # not a DTA file... should return something more useful/verbose than this...
                context['properties'] = [('ERROR', 'file_type not "DTA"')]
                return context

        context['properties'].append(('Parsed?', dta_record.parsed))
        if dta_record.parsed:
            context['parsed'] = True
            context['properties'].append(('Parsed file path', dta_record.parsed_json.url))
            context['properties'].append(('JSON output', self.request.path+'json'))
        context['properties'].append(('Last processed', dta_record.last_processed))

        return context

class DTAParseView(DocumentDetailView):
    
    """View that parses DTASelect file (if necessary)
    """

    model = Document
    #template_name = 'analysis/document_dta_detail.html'
    template_name = 'data/dta_parse_success.html'

    def get_context_data(self, **kwargs):
        context = super(DocumentDetailView, self).get_context_data(**kwargs)

        if context['object'].file_type != 'DTA':
            # not a DTA file? return immediately...
            return context
        else:
            import io
            import json
            from .parsers import dta
            from django.core.files import File
            try:
                dta_record = DTAFile.objects.get(pk=self.kwargs['pk'])
            except DTAFile.DoesNotExist:
                # shouldn't get here...
                raise
                return context

            file_name = context['object'].docfile.name
            file_path_full = str(context['object'].docfile.file)
            parsed_dta_dict = list(dta.dta_select_parser(file_path_full))

            f = io.StringIO()
            f.write(json.dumps(parsed_dta_dict))
            django_file = File(f)
            dta_record.parsed_json.save(file_name+'.json', django_file)
            dta_record.parsed = True
            dta_record.save()
            f.close()
            context['dtafile_parsed_file'] = dta_record.parsed_json.url
            print(context.keys())

            return context

class DTAJSONView(APIView):
    
    """View that returns a JSON response using parsed
        DTASelect-filter.txt (DTAFile) output
    """

    renderer_classes = (JSONRenderer, )

    def get(self, request, pk, format=None):
        
        try:
            dta_record = DTAFile.objects.get(pk=pk)
        except DTAFile.DoesNotExist:
            return RestResponse({'file':'not found'}, status=rest_status.HTTP_404_NOT_FOUND)

        if not dta_record.parsed:
            return RestResponse({'file':'not yet parsed'}, status=rest_status.HTTP_412_PRECONDITION_FAILED)

        import json
        json_file_path = str(dta_record.parsed_json.file)
        with open(json_file_path) as f:
            content = json.load(f)

        return RestResponse(content)

class SQTDetailView(DocumentDetailView):

    """DetailView for SQT files
    """

    template_name_suffix = '_sqt_detail'

    def get_context_data(self, **kwargs):
        context = super(SQTDetailView, self).get_context_data(**kwargs)

        context['properties'].append(('# scans', self.get_number_scans(context['object'].docfile.url)))

        return context

    def get_number_scans(self, file_path):
        # some code to count # scans
        return 0

class MS1DetailView(DocumentDetailView):

    """DetailView for MS1 files
    """

    template_name_suffix = '_ms1_detail'

class MS2DetailView(DocumentDetailView):

    """DetailView for MS2 files
    """

    template_name_suffix = '_ms2_detail'
